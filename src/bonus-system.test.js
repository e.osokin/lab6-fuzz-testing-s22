import { calculateBonuses } from "./bonus-system.js";
const assert = require("assert");


describe('Calculator tests', () => {
    let app;
    console.log("Tests started");

    test('Standard, 1', (done) => {
        expect(calculateBonuses('Standard', 1)).toEqual(0.05);
        done()
    })


    test('Premium, 1', (done) => {
        expect(calculateBonuses('Premium', 1)).toEqual(0.1);
        done()
    })

    test('Diamond, 1', (done) => {
        expect(calculateBonuses('Diamond', 1)).toEqual(0.2);
        done()
    })

    test('Standard, 10000', (done) => {
        expect(calculateBonuses('Standard', 10000)).toEqual(0.07500000000000001);
        done()
    })


    test('Premium, 10000', (done) => {
        expect(calculateBonuses('Premium', 10000)).toEqual(0.15000000000000002);
        done()
    })

    test('Diamond, 10000', (done) => {
        expect(calculateBonuses('Diamond', 10000)).toEqual(0.30000000000000004);
        done()
    })

    test('Standard, 50000', (done) => {
        expect(calculateBonuses('Standard', 50000)).toEqual(0.1);
        done()
    })


    test('Premium, 50000', (done) => {
        expect(calculateBonuses('Premium', 50000)).toEqual(0.2);
        done()
    })

    test('Diamond, 50000', (done) => {
        expect(calculateBonuses('Diamond', 50000)).toEqual(0.4);
        done()
    })




    test('Standard, 15000', (done) => {
        expect(calculateBonuses('Standard', 15000)).toEqual(0.07500000000000001);
        done()
    })


    test('Premium, 15000', (done) => {
        expect(calculateBonuses('Premium', 15000)).toEqual(0.15000000000000002);
        done()
    })

    test('Diamond, 15000', (done) => {
        expect(calculateBonuses('Diamond', 15000)).toEqual(0.30000000000000004);
        done()
    })



    test('Standard, 75000', (done) => {
        expect(calculateBonuses('Standard', 75000)).toEqual(0.1);
        done()
    })


    test('Premium, 75000', (done) => {
        expect(calculateBonuses('Premium', 75000)).toEqual(0.2);
        done()
    })

    test('Diamond, 75000', (done) => {
        expect(calculateBonuses('Diamond', 75000)).toEqual(0.4);
        done()
    })


    test('Standard, 750000', (done) => {
        expect(calculateBonuses('Standard', 750000)).toEqual(0.125);
        done()
    })


    test('Premium, 750000', (done) => {
        expect(calculateBonuses('Premium', 750000)).toEqual(0.25);
        done()
    })

    test('Diamond, 750000', (done) => {
        expect(calculateBonuses('Diamond', 750000)).toEqual(0.5);
        done()
    })

    test('Standard, 100000', (done) => {
        expect(calculateBonuses('Standard', 100000)).toEqual(0.125);
        done()
    })


    test('Premium, 100000', (done) => {
        expect(calculateBonuses('Premium', 100000)).toEqual(0.25);
        done()
    })

    test('Diamond, 100000', (done) => {
        expect(calculateBonuses('Diamond', 100000)).toEqual(0.5);
        done()
    })


    test('Invalid, 1', (done) => {
        expect(calculateBonuses('Invalid', 1)).toEqual(0);
        done()
    })


    test('Invalid, 1', (done) => {
        expect(calculateBonuses('Invalid', 1)).toEqual(0);
        done()
    })

    test('Invalid, 1', (done) => {
        expect(calculateBonuses('InvalidInvalid', 1)).toEqual(0);
        done()
    })

    console.log('Tests Finished');

});
